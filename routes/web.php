<?php

Route::get('/', 'HomeController@index');
Route::get('/admin', 'AdminController@index');
Route::post('/admin', 'AdminController@login');
