<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" type="image" href="images/cendol-nude.jpeg">
    <link href="{{ asset('/css/app.css') }}" rel="stylesheet"/>
    <script src="{{ asset('/js/app.js') }}"></script>
    <title>Reymi</title>
  </head>
  <body>
    @include('shared.navbar')
    @yield('content')
  </body>
</html>
