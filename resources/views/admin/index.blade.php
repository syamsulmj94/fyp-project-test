@extends('master')

@section('content')
  <div class="jumbotron">
    <div class="container">
      @if (session('success'))
        <div class="alert alert-success">
          {{ session('success') }}
        </div>
      @endif
      @if ($errors->any())
        <div class="alert alert-danger">
          <ul>
            @foreach ($errors->all() as $error)
              <li>
                {{ $error }}
              </li>
            @endforeach
          </ul>
        </div>
      @endif
      <h1>Login</h1>
      <div>
        <form action="{{ action('AdminController@login') }}" method="post">
          {{ csrf_field() }}
          <div class="input-group">
            <label>Username</label>
            <input name="username" placeholder="Username" class="form-control"/>
          </div>
          <div class="input-group">
            <label>Password</label>
            <input name="password" placeholder="Password" class="form-control" type="password"/>
          </div>
          <div class="nak-margin">
            <a href="#" class="btn btn-info">Register</a>
            <button class="btn btn-primary" type="submit">Login</button>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection
